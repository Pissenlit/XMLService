/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stbcore.exchange;

/**
 * Possible error types.
 * @author Axelle Boucher
 */
public enum ErrorType {
    NO_SUCH_STB("No such stb"),
    INCORRECT_DATA_FORMAT("Incorrect data format");
    
    private String message;
    
    private ErrorType(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
    
}
