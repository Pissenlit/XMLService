/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stbcore.exchange;

/**
 *
 * @author Axelle Boucher
 */
public enum RequestType {
    GET_HOME_MESSAGE("/", "GET"),
    GET_STB_BY_ID("/STB/resume/{0}", "GET"),
    GET_ALL_STB("/STB/resume", "GET"),
    DELETE_STB_BY_ID("/STB/delete/{0}", "DELETE"),
    DELETE_ALL_STB("/STB/delete", "DELETE"),
    INSERT_STB("/STB/insert"," POST");

    private static final String URL = "http://localhost:8082/STBService";

    private final String method;
    private final String url;

    private RequestType(String url, String method) {
        this.method = method;
        this.url = URL + url;
    }

    /**
     * Inserts parameters value in the URL of this type. 
     * @param params
     * The parameters to insert.
     * @return The built URL.
     */
    public String getURL(Object... params) {
        String str = url;
        for (int i = 0; i < params.length; ++i) {
            str = str.replace("{" + i + "}", params[i].toString());
        }
        return str;
    }

    /**
     * Returns the method of this type.
     * @return The method of this type.
     */
    public String getHttpMethod() {
        return method;
    }
}
