/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stbcore.exchange;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import stbcore.model.StbList;

/**
 *
 * @author Axelle Boucher
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class StbListResponse {
    @XmlElement
    private StbList stbList;
    @XmlElement
    private StbError error;

    public StbList getStbList() {
        return stbList;
    }

    public void setStbList(StbList stbList) {
        this.stbList = stbList;
    }

    public StbError getError() {
        return error;
    }
    
   public boolean hasError() {
       return getError() != null;
   }

    public void setError(StbError error) {
        this.error = error;
    }
    
    
}
