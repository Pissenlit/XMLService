package stbcore.model;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class SimpleErrorHandler implements ErrorHandler {
    private List<String> errorMessages;

    public SimpleErrorHandler() {	
            errorMessages = new ArrayList<String>();
    }

    public boolean hasError() {
            return !errorMessages.isEmpty();
    }

    public String getErrorMessage() {
            String errorMsg = "";
            for(String s : errorMessages) {
                    errorMsg = errorMsg + s + "\n";
            }
            return errorMsg;
    }
	
    public void warning(SAXParseException e) throws SAXException {
    	this.errorMessages.add("Warning : " + e.getMessage());
    }

    public void error(SAXParseException e) throws SAXException {
    	this.errorMessages.add("Error : " + e.getMessage());
    }

    public void fatalError(SAXParseException e) throws SAXException {
    	this.errorMessages.add("Fatal error : " + e.getMessage());
    }
}		