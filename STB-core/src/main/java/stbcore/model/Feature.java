/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stbcore.model;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * A STB feature.
 * @author Axelle Boucher
 */
@XmlRootElement(name = "feature")
@XmlAccessorType(XmlAccessType.FIELD)
public class Feature {
    @XmlElement(name = "description")
    private String description;
    @XmlElement(name = "priority")
    private String priority;
    @XmlElement(name = "functionnalRequirement")
    private List<FunctionnalRequirement> functionnalRequirements;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public List<FunctionnalRequirement> getFunctionnalRequirements() {
        return functionnalRequirements;
    }

    public void setFunctionnalRequirements(List<FunctionnalRequirement> functionnalRequirements) {
        this.functionnalRequirements = functionnalRequirements;
    }
    
    
}
