/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stbcore.model;

import java.util.ArrayList;
import java.util.List;
import stbcore.tools.RandomString;

/**
 *
 * @author Axelle Boucher
 */
public class RandomStbGenerator {
    /**
     * Creates a stb with random field values.
     * @return 
     */
    public Stb generateRandomSTB() {
        RandomString randomString = new RandomString(10);
        
        Stb stb = new Stb();
        
        //basic info
        stb.setId(randomString.nextString());
        stb.setTitle("A random STB");
        stb.setDescription(randomString.nextString());
        stb.setDate("2016-05-06");
        stb.setVersion("1.0");
        
        //client info
        Client client = new Client();
        client.setEntity("UFR Rouen");
        client.setZipcode("76000");
        Person contact = new Person();
        contact.setFirstName(randomString.nextString());
        contact.setLastName(randomString.nextString());
        client.setContact(contact);
        stb.setClient(client);
        
        //team info
        Team team = new Team();
        List<Person> members = new ArrayList<>();
        for (int i = 0; i < 3; ++i) {
            Person member = new Person();
            member.setFirstName(randomString.nextString());
            member.setLastName(randomString.nextString());
            members.add(member);
        }
        team.setMembers(members);
        stb.setTeam(team);
        
        //features info
        List<Feature> features = new ArrayList<>();
        for (int i = 1; i < 3; ++i) {
            Feature feature = new Feature();
            feature.setDescription(randomString.nextString());
            feature.setPriority("High");
            List<FunctionnalRequirement> functionnalRequirements = new ArrayList<>();
            for (int j = 1; j < 3; ++j) {
                FunctionnalRequirement functionnalRequirement = new FunctionnalRequirement();
                functionnalRequirement.setId("FR_" + j);
                functionnalRequirement.setDescription(randomString.nextString());
                functionnalRequirement.setPriority("Average");
                functionnalRequirements.add(functionnalRequirement);
            }
            feature.setFunctionnalRequirements(functionnalRequirements);
            features.add(feature);
        }
        stb.setFeatures(features);
        return stb;
    }
}
