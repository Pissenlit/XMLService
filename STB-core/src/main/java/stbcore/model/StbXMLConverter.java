/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stbcore.model;

import java.io.StringReader;
import java.io.StringWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author Axelle Boucher
 */
public class StbXMLConverter {
    
    private final JAXBContext jaxbContext;

    public StbXMLConverter() throws JAXBException {
        jaxbContext =  JAXBContext.newInstance(Stb.class);
    }
    
    public String getXMLFromStb(Stb stb) throws JAXBException {
        return getXMLFromStb(stb, false);
    }
    
    public String getXMLFromStb(Stb stb, boolean pretty) throws JAXBException {
        Marshaller marshaller  = jaxbContext.createMarshaller();
        if (pretty) {
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        }
        StringWriter writer = new StringWriter();
        marshaller.marshal(stb, writer);
        String stbXML = writer.toString();
        return stbXML;
    }
    
    public Stb getStbFromXML(String stbXML) throws JAXBException {
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        Stb stb = (Stb) unmarshaller.unmarshal(new StringReader(stbXML));
        return stb;
    }
}
