/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stbcore.model;

import stbcore.model.SimpleErrorHandler;
import java.io.IOException;
import java.io.StringReader;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/**
 *
 * @author Axelle Boucher
 */
public class STBValidator {
    
    private String xsdLocation;
    
    private String lastErrorMessage;

    public String getXsdLocation() {
        return xsdLocation;
    }

    public String getLastErrorMessage() {
        return lastErrorMessage;
    }
    
    public void setXsdLocation(String xsdLocation) {
        this.xsdLocation = xsdLocation;
    }
    
    public boolean isValidSTB(String xmlStb) {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setValidating(false);
        factory.setNamespaceAware(true);

        SchemaFactory schemaFactory = 
            SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");

        try {
            factory.setSchema(schemaFactory.newSchema(
                new Source[] {new StreamSource(getXsdLocation())}));

            SAXParser parser = factory.newSAXParser();

            XMLReader reader = parser.getXMLReader();

            SimpleErrorHandler errHandler = new SimpleErrorHandler();
            reader.setErrorHandler(errHandler);
            reader.parse(new InputSource(new StringReader(xmlStb)));
            this.lastErrorMessage = errHandler.getErrorMessage();
            return !errHandler.hasError();
        } catch (ParserConfigurationException pce) {
                pce.printStackTrace();
        } catch (IOException ioe) {
                ioe.printStackTrace();
        } catch (SAXException se) {
                se.printStackTrace();
        }
        return false;
    }
}
