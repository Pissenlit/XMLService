/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stbcore.model;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Axelle Boucher
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class StbList {
    @XmlElement(name="stb")
    private List<Stb> STBs;

    public List<Stb> getSTBs() {
        return STBs;
    }

    public void setSTBs(List<Stb> STBs) {
        this.STBs = STBs;
    }

    @Override
    public String toString() {
        return STBs.toString();
    }
    
    
}
