package stbcore.model;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * A class to represent a Stb according to stb.xsd schema.
 *
 * @author Axelle Boucher
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(namespace="http://univ.fr/stb")
public class Stb {
    @XmlElement
    public String id;
    @XmlElement
    private String title;    
    @XmlElement
    private String version;
    @XmlElement
    private String date;
    @XmlElement
    private String description;
    @XmlElement
    private Client client;
    @XmlElement 
    private Team team;
    @XmlElement(name = "feature")
    private List<Feature> features;

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    } 

    public String getDate() {
        return date;
    }
    
    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(List<Feature> features) {
        this.features = features;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }    

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "STB - "
                + "id="
                + (id != null ? id : "null")
                + " "
                + "title="
                + (title != null ? title : "null")
                + " "
                + "date="
                + (date != null ? date : "null")
                + " "
                + "description="
                + (description != null ? description : "null");
    }

}
