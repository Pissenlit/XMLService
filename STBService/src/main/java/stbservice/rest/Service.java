/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stbservice.rest;

import java.util.ArrayList;
import org.springframework.data.mongodb.core.MongoTemplate;
import stbcore.model.Stb;

/**
 *
 * @author Axelle Boucher
 */
public class Service {
    private final MongoTemplate mongoTemplate;
    
    public Service(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }
    
    public MongoTemplate getMongoTemplate() {
        return mongoTemplate;
    }
    
    /**
     * Returns a list of all stb in the database.
     * @return 
     */
    public ArrayList<Stb> getAllSTBs() {
        return new ArrayList(mongoTemplate.findAll(Stb.class));
    }
    
    /**
     * Returns the stb of given id.
     * @param id
     * @return 
     */
    public Stb getSTB(String id) {
        return mongoTemplate.findById(id, Stb.class);
    }
    
    /**
     * Insert a new stb in the database.
     * @param stb 
     */
    public void insertSTB(Stb stb) {
        mongoTemplate.save(stb);
    }
    
    /**
     * Deletes a stb of given id.
     * @param id 
     */
    public boolean deleteSTB(String id) {
        System.out.println("Deleting stb of id : " + id);
        Stb stb = mongoTemplate.findById(id, Stb.class);
        System.out.println("Found stb to delete");
        System.out.println(stb);
        if (stb != null) {
            System.out.println("Will delete it");
            mongoTemplate.remove(stb);
            System.out.println("Deleted");
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Deletes all stbs.
     */
    public boolean deleteAllSTBs() {
        if (getAllSTBs().size() > 0) {
            mongoTemplate.dropCollection(Stb.class);
            return true;
        } else {
            return false;
        }
    }
    
}
