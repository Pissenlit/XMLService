package stbservice.log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Logger {
	private File destination;
	private BufferedWriter writer;
	
	public Logger() {
		
	}

	public File getDestination() {
		return destination;
	}

	public void setDestination(File destination) {
		try {
			this.destination = destination;
			this.writer = new BufferedWriter(new FileWriter(destination));
		} catch (IOException ioe) {
			
		}
	}
	
	public void log(Exception e) {
		try {
			writer.append("[Exception] " + e.getClass().getName() + " " + e.getMessage() + "\n");
		} catch (IOException ioe) {
			
		}
	}
	
	public void log(String message) {
		try {
			writer.append("[Message] " + message + "");
		} catch (IOException ioe) {
			
		}
	}
}
