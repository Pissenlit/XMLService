package stbservice.controller;

import java.io.StringReader;
import java.util.ArrayList;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import stbcore.exchange.StbError;
import stbcore.model.RandomStbGenerator;
import stbcore.exchange.StbResponse;
import stbcore.model.STBValidator;
import stbcore.model.Stb;
import stbcore.model.StbList;
import stbcore.exchange.StbListResponse;


@Controller
@RequestMapping("/")
public class STBController extends AbstractController {
    private static final String XSD_LOCATION = "/home/axelle/Documents/MS2/LW2/XMLService/STBService/src/main/resources/stb.xsd";
    
    @Autowired
    private JAXBContext jaxbContext;
    
    /**
     * Handles get request to get all stb.
     * @return 
     */
    @RequestMapping(method = RequestMethod.GET, value = "/STB/resume", produces = "application/xml")
    public @ResponseBody StbListResponse getAllSTBs() {
        ArrayList<Stb> allSTBs = getService().getAllSTBs();
        StbList stbList = new StbList();
        stbList.setSTBs(allSTBs);
        StbListResponse response = new StbListResponse();
        response.setStbList(stbList);
        return response;
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "STB/resume/{id}", produces = "application/xml")
    public @ResponseBody StbResponse getSTB(@PathVariable String id) {
        System.out.println("Getting stb of id " + id);
        Stb stb = getService().getSTB(id);
        StbResponse response = new StbResponse();
        if (stb != null) {
            System.out.println("Found stb in database");
            response.setStb(stb);
        } else {
            System.out.println("Did not find stb in database");
            response.setError(new StbError(
                        stbcore.exchange.ErrorType.NO_SUCH_STB,
                        "This id does not match any stb"
            ));
        }
        return response;     
    }

    /**
     * Handles post method to insert a stb.
     * @param xmlStb
     * @return 
     */
    @RequestMapping(method = RequestMethod.POST, value = "STB/insert")
    public @ResponseBody StbResponse insertSTB(@RequestBody String xmlStb) {
        System.out.println("Inserting STB : ");
        System.out.println(xmlStb);
        
        StbResponse stbResponse = new StbResponse();
        
        STBValidator stbValidator = new STBValidator();
        stbValidator.setXsdLocation(XSD_LOCATION);
        
        /**
         * Probleme avec la validation : le contenu xml doit contenir les infos
         * de schema dans la balise stb : 
         * <p:stb xmlns:p="http://univ.fr/stb" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://univ.fr/stb stb.xsd ">
         * ...
         * </p:stb>
         */
        if (stbValidator.isValidSTB(xmlStb)) {
            try {
                Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
                Stb stb = (Stb) unmarshaller.unmarshal(new StringReader(xmlStb));
                getService().insertSTB(stb);
                stbResponse.setStb(stb);
            } catch (JAXBException je) {
                stbResponse.setError(new StbError(
                        stbcore.exchange.ErrorType.INCORRECT_DATA_FORMAT,
                        "Impossible to parse this xml content"
                ));
            }   
        } else {
            stbResponse.setError(new StbError(
                        stbcore.exchange.ErrorType.INCORRECT_DATA_FORMAT,
                        stbValidator.getLastErrorMessage()
                ));
        }
        return stbResponse;
    }
    
    /**
     * Handles delete request to delete all stbs.
     * @return 
     */
    @RequestMapping(method = RequestMethod.DELETE, value = "/STB/delete")
    public @ResponseBody String deleteAllSTBs() {
        boolean deleted = service.deleteAllSTBs();
        return deleted ? "All stbs were deleted" : "No stb was deleted";
    }
    
    /**
     * Handles delete request to delete a stb of given id.
     * @param id
     * @return 
     */
    @RequestMapping(method = RequestMethod.DELETE, value = "/STB/delete/{id}")
    public @ResponseBody String deleteSTB(@PathVariable String id) {
        boolean deleted = service.deleteSTB(id);       
        return deleted ? "One stb was deleted" : "No stb was deleted";
    }
    
    /**
     * Creates a display a random stb.
     * @return 
     */
    @RequestMapping(method = RequestMethod.GET, value = "/STB/random", produces="application/xml")
    public @ResponseBody StbResponse getRandomSTB() {
        Stb stb = new RandomStbGenerator().generateRandomSTB();
        StbResponse stbResponse = new StbResponse();
        stbResponse.setStb(stb);
        return stbResponse;
    }
    
}


