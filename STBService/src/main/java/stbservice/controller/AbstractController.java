/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stbservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import stbservice.rest.Service;

/**
 *
 * @author Axelle Boucher
 */
@Controller
public abstract class AbstractController {
    @Autowired
    protected Service service;
    
    public Service getService() {
        return service;
    }
}
