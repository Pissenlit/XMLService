package stbservice.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import stbcore.exchange.Message;

@Controller
@RequestMapping("/")
public class HomeController extends AbstractController {
       
    @RequestMapping(method = RequestMethod.GET, produces = "application/xml") 
    public @ResponseBody Message welcome() {
        Message message = new Message();
        message.setMessage("User's manual : <br>"
                            + "GET /STB/resume : to view a list of all STBs<br>"
                            + "GET /STB/resume/{id} : to get the content of a STB with given id<br>"
                            + "POST /STB/insert : to insert a STB contained in request body<br>"
                            + "There are currently "
                            + getService().getAllSTBs().size()
                            + " stb in the database.");
        return message;
    }  
}
