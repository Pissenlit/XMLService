/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stbclient.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import stbcore.exchange.Message;
import stbcore.exchange.RequestType;

/**
 * Home controller
 * @author Axelle Boucher
 */
@Controller
@RequestMapping(path="/")
public class HomeController extends AbstractController {
    
    /**
     * Handles get request : show home page.
     */
    @RequestMapping(method = RequestMethod.GET)
    public String showIndex(Model model) {
        Message homeMessage = restTemplate.getForObject(
                RequestType.GET_HOME_MESSAGE.getURL(),
                Message.class
        );
        model.addAttribute("homeMessage", homeMessage.getMessage());
        return "index";
    }
}
