/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stbclient.controllers;

import javax.xml.bind.JAXBException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import stbcore.exchange.RequestType;
import stbcore.exchange.StbResponse;
import stbcore.model.Stb;
import stbcore.model.StbList;
import stbcore.model.StbXMLConverter;
import stbcore.exchange.StbListResponse;

/**
 * Controller that handles stb page.
 * @author Axelle Boucher
 */
@Controller
@RequestMapping(value="viewSTB")
public class ViewStbController extends AbstractController {
    
    /**
     * Handles GET method to display a STB whose id is given as path variable.
     * @param model
     * @param id
     * @param redir
     * @return 
     */
    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public String displayStbPageById(Model model, 
            @PathVariable String id,
            RedirectAttributes redir) {
        //Get the stb

        StbResponse response = restTemplate.getForObject(
                RequestType.GET_STB_BY_ID.getURL(id), 
                StbResponse.class
        );
        
        if (!response.hasError()) {
            Stb stb = response.getStb();
            model.addAttribute("stb", stb);//Compute the xml string for the stb to display it as well.
            try {
                String stbXML = new StbXMLConverter().getXMLFromStb(stb);
                System.out.println(stbXML);
                String stbXMLPretty = pretty(stbXML);

                model.addAttribute("stbXML", stbXMLPretty);
            } catch (JAXBException jaxbe) {
                model.addAttribute("stbXML", "XML content unavailable");
            }
            return "viewSTB";
        } else {
            redir.addFlashAttribute("error", response.getError());
            return "redirect:/error";
        }
    }
    
    /**
     * Handles GET method to display all STBs.
     * @param model
     * @return 
     */
    @RequestMapping(method = RequestMethod.GET, value = "/all")
    public String displayAllSTBsPage(Model model) {
        RestTemplate restTemplate = new RestTemplate();
        StbListResponse  response = restTemplate.getForObject(
                RequestType.GET_ALL_STB.getURL(), 
                StbListResponse.class
        );
        if (!response.hasError()) {
            StbList stbList = response.getStbList();
            model.addAttribute("allStbs", stbList);
            return "viewAllSTBs";
        } else {
            return "redirect:/error";
        }
        
    }
    
    private String pretty(String xml) {
        StringBuilder stringBuilder = new StringBuilder();
        System.out.println(xml.length());
        int inf = 0;
        int openTag = xml.indexOf("</");
        while (openTag > 0) {
            int closeTag = xml.indexOf(">", openTag);
            System.out.println("inf : " + inf + ", open : " + openTag + ", close : " + closeTag);
            stringBuilder.append(xml.substring(inf, closeTag + 1));
            stringBuilder.append("\r\n");
            inf = closeTag + 1;
            openTag = xml.indexOf("</", inf);
        }
        System.out.println(stringBuilder.toString());
        return stringBuilder.toString();
    }
}
