/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stbclient.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import stbcore.exchange.RequestType;

/**
 *
 * @author Axelle Boucher
 */
@Controller
@RequestMapping(value = "/deleteSTB")
public class DeleteSTBController extends AbstractController {
    
    /**
     * Handles GET method to delete a STB whose id is given as path variable.
     * @param model
     * @param id
     * @return 
     */
    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public String deleteSTB(@PathVariable String id) {
        restTemplate.delete(RequestType.DELETE_STB_BY_ID.getURL(id));
        return "redirect:/viewSTB/all";
    }
    
    /**
     * Handles GET method to delete all STBs.
     * @param model
     * @param id
     * @return 
     */
    @RequestMapping(method = RequestMethod.GET, value = "/all")
    public String deleteAllSTBs() {
        restTemplate.delete(RequestType.DELETE_ALL_STB.getURL());
        return "redirect:/viewSTB/all";
    }
}
