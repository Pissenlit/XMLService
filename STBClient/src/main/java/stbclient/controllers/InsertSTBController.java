/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stbclient.controllers;

import java.sql.Date;
import javax.xml.bind.JAXBException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import stbcore.exchange.ErrorType;
import stbcore.exchange.RequestType;
import stbcore.exchange.StbError;
import stbcore.exchange.StbResponse;
import stbcore.tools.IdGenerator;
import stbcore.model.Stb;
import stbcore.model.StbXMLConverter;

/**
 * Controller that handles insertion page.
 * @author Axelle Boucher
 */
@Controller
@RequestMapping(path="/insertSTB")
public class InsertSTBController extends AbstractController {
    
    @Autowired
    public IdGenerator idGenerator;
    @Autowired
    public StbXMLConverter converter;
    
    @RequestMapping(method = RequestMethod.GET)
    public String displayInsertionPage(Model model) {
        Stb stb = new Stb();
        model.addAttribute("stb", stb);
        return "insertSTB";
    }
    
    @RequestMapping(path="/form", method = RequestMethod.POST)
    public String insertSTB(Model model,
            @ModelAttribute Stb stb, 
            @RequestParam(value = "date", required = false) String date,
            RedirectAttributes redir) {
        stb.setId(idGenerator.nextId());
        System.out.println(stb);
        try {
            System.out.println(new StbXMLConverter().getXMLFromStb(stb));
        } catch(JAXBException je) {
            System.out.println("impossible to display xml");
        }
        return sendStb(redir, stb);
    }
    
    @RequestMapping(path = "/directInput", method = RequestMethod.POST)
    public String insertSTB(Model model,
            @RequestParam(value = "stbInput") String stbInput,
            RedirectAttributes redir) {
        try {

            Stb stb = converter.getStbFromXML(stbInput);
            stb.setId(idGenerator.nextId());
            return sendStb(redir, stb);

        } catch (JAXBException jaxbe) {
            System.out.println("Caught JAXBException");
            redir.addFlashAttribute(
                    "error",
                    new StbError(
                            ErrorType.INCORRECT_DATA_FORMAT,
                            "Unable to unmarshal object"
                    )
            );

            return "redirect:/insertSTB";
        }
    }
    
    /**
     * Sends an STB.
     * @param stb
     * @return 
     */
    private String sendStb(RedirectAttributes redir, Stb stb) {
        
        StbResponse stbResponse = restTemplate.postForObject(
                RequestType.INSERT_STB.getURL(), 
                stb, 
                StbResponse.class);
        
        if (!stbResponse.hasError()) {
            redir.addFlashAttribute("successMessage", "The stb was successfuly created");
            return "redirect:/viewSTB/" + stb.getId();
        } else {
            redir.addFlashAttribute("error", stbResponse.getError());
            return "redirect:/insertSTB";
        }

    }
}
