/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stbclient.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Axelle Boucher
 */
@Controller
@RequestMapping(value = "search")
public class SearchController extends AbstractController {
    @RequestMapping(method = RequestMethod.POST)
    public String search(@RequestParam(value = "searchContent") String searchContent) {
        return "redirect:viewSTB/" + searchContent;  
    }
}
