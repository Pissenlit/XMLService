<%-- 
    Document   : footer.js
    Created on : May 6, 2016, 7:58:45 PM
    Author     : Axelle Boucher
--%>


<footer class = "footer navbar navbar-default col-md-12">
    <div class = "row"
        <div class = "col-md-offset-7 col-md-4">
            By : 
            <ul>
                <li>Hugo MOCHET</li>
                <li>Axelle BOUCHER</li>
            </ul>
        </div>
    </div>   
</footer>