<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- JQuery -->      
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Bootstrap JS -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<!-- Bootstrap css -->
<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

<!--Custom stylesheets -->
<link href="<c:url value="/resources/stylesheets/code.css"/>" rel="stylesheet"  type="text/css">

<!-- JS files -->
<script src="${pageContext.request.contextPath}/resources/js/MoreOfThisButton.js"></script>