<%-- 
    Document   : viewAllSTB
    Created on : May 2, 2016, 2:47:59 PM
    Author     : Axelle Boucher
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="stbcore.model.Stb"%>
<%@page import="stbcore.model.StbList"%>

<%
    StbList allStbs = (StbList) request.getAttribute("allStbs");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@include file="includes/includes.jsp" %>
        <title>JSP Page</title>
    </head>
    <body>
        <div id = "page" class = "container">
        <%@include file="includes/header.jsp" %>    
        
            <h1>All STBs</h1>
            <table id = "allSTBs"
                   class = "table table-striped">
                <c:forEach items="${allStbs.getSTBs()}" var="stb">
                    <tr>
                        <td>
                            <div class = "row">
                                <div class= "col-md-4">Id :</div>
                                <div class= "col-md-6"><c:out value="${stb.getId()}"/></div>
                            </div >
                            <div class = "row">
                                <div class= "col-md-4">Title :</div>
                                <div class= "col-md-6"><c:out value="${stb.getTitle()}"/></div>
                            </div >
                            <div class = "row">
                                <div class= "col-md-4">Version :</div>
                                <div class= "col-md-6"><c:out value="${stb.getVersion()}"/></div>
                            </div >
                            <div class = "row">
                                <div class= "col-md-4">Description :</div>
                                <div class= "col-md-6"><c:out value="${stb.getDescription()}"/></div>
                            </div >
                        </td>
                        <td>
                            <a href = "${pageContext.request.contextPath}/viewSTB/${stb.getId()}" >
                                <button class = "btn btn-default">View</button>
                            </a>
                        </td>
                        <td>
                            <a href = "${pageContext.request.contextPath}/deleteSTB/${stb.getId()}" >
                                <button class = "btn btn-default">Delete</button>
                            </a>
                        </td>
                    </tr>
                </c:forEach>
            </table>
            
            <a href="${pageContext.request.contextPath}/deleteSTB/all">
                <button class = "btn btn-default">Delete all</button>
            </a>

        </div>
    </body>
</html>
