<%-- 
    Document   : index
    Created on : Apr 26, 2016, 8:47:50 PM
    Author     : Axelle Boucher
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@include file="includes/includes.jsp" %>
        <title>JSP Page</title>
    </head>
    <body>
        
        <div id = "page" class = "container">     
        <%@include file="includes/header.jsp" %>
        
                <div class = "row"> 
                    <h1 class = "col-md-offset-4">Welcome home</h1>
                </div>
                <div class = "row"> 
                    <div class = "col-md-8 col-md-offset-2 panel panel-default">
                        <p class = "lead">This web client was developped by Axelle BOUCHER and Hugo MOCHET.</p>
                    </div>
                </div>
        </div>
    </body>
    
</html>
