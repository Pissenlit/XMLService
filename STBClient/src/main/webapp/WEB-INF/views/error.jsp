<%-- 
    Document   : index
    Created on : Apr 26, 2016, 8:47:50 PM
    Author     : Axelle Boucher
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@include file="includes/includes.jsp" %>
        <title>JSP Page</title>
    </head>
    <body>
        
        <div id = "page" class = "container">     
        <%@include file="includes/header.jsp" %>
        
        <%@include file="includes/contextualMessage.jsp" %>
        
        </div>
    </body>
    
</html>
