// Manage direct input content : put a template
var stbInputContent = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
    + "\n\n"
    + "<p:stb xmlns:p=\"http://univ.fr/stb\""
    + " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
    + " xsi:schemaLocation=\"http://univ.fr/stb stb.xsd\">"
    + "\n\n"
    + "<title></title>"
    + "\n"
    + "<version></version>"
    + "\n"
    + "<date></date>"
    + "\n"
    + "<description></description>"
    + "\n\n"
    + "<client>"
    + "\n"
    + "<entity></entity>"
    + "\n"
    + "<contact>"
    + "\n"
    + "<firstName></firstName><lastName></lastName>"
    + "\n"
    + "</contact>"
    + "\n"
    + "<zipcode></zipcode>"
    + "\n"
    + "</client>"
    + "\n\n"
    + "<team>"
    + "\n"
    + "<member>"
    + "\n"
    + "<firstName></firstName><lastName></lastName>"
    + "\n"
    + "</member>" 
    + "\n"
    + "</team>"  
    + "\n\n"
    + "<feature>"
    + "\n"
    + "<description></description>"
    + "\n"
    + "<priority>High</priority>"
    + "\n"
    + "<functionnalRequirement>"
    + "\n"
    + "<id></id>"
    + "\n"
    + "<description></description>"
    + "\n"
    + "<priority>Average</priority>"
    + "\n"
    + "</functionnalRequirement>"
    + "\n"
    + "</feature>"
    + "\n\n"
    + "</p:stb>"
$("#stbInput").prop("value", stbInputContent);


//Members input

var membersNb = 0;

var createNewMemberInput = function() {
    $newMember = $("<div></div>");
    $newMember.prop("class", "form-group row member col-md-12")
    $newMember.html("<input name = \"team.members[" + membersNb + "].firstName\""
            + " class = \"col-md-6 form-control\""
            + " placeholder = \"First name\"/>"
            + "<input name = \"team.members[" + membersNb + "].lastName\""
            + " class = \"col-md-6 form-control\""
            + " placeholder = \"Last name\"/>");
    ++membersNb;
    return $newMember;
};

var moreMembers = new MoreOfThisButton($("#team"), createNewMemberInput);

// Functionnal requirements inputs

var frNb = 0;
var createNewFunctionnalRequirementInput = function() {
    var $newFR = $("<div></div>")
    $newFR.prop("class", "panel panel-default")
    $newFR.html(
            "<div class = \"row\">"
                + "<label name = \"features[" + featuresNb + "].functionnalRequirements[" + frNb + "].id\""
                + " class = \"col-md-4\">"
                    + "Id :"
                + "</label>"
                + "<input name = \"features[" + featuresNb + "].functionnalRequirements[" + frNb + "].id\""
                + " class = \"col-md-2 form-control\"/>"
            + "</div>"
            + "<div class = \"row\">"
                + "<label name = \"features[" + featuresNb + "].functionnalRequirements[" + frNb + "].priority\""
                + " class = \"col-md-4\">"
                    + "Priority :"
                + "</label>"
                + "<select name = \"features[" + featuresNb + "].functionnalRequirements[" + frNb + "].priority\""
                + " class = \"col-md-2 form-control\">"
                + "</select>"
            + "</div>"
            + "<div class = \"row\">"
                + "<label name = \"features[" + featuresNb + "].functionnalRequirements[" + frNb + "].description\""
                + " class = \"col-md-4\">"
                + "Description :"
                + "</label>"
                + "<textarea name = \"features[" + featuresNb + "].functionnalRequirements[" + frNb + "].description\""
                + " class = \"col-md-8 form-control\""
                + " placeholder = \"Description\"/>"
            + "</div>"
    );

    var $select = $newFR.find("select");
    var priorities = ["High", "Average", "Low"];
    $.each(priorities, function(index, elt) {
        var $option = $("<option></option>");
        $option.prop("value", elt)
        $option.html(elt);
        $select.append($option);
    });
    return $newFR
}
// Features inputs

var featuresNb = 0;

var createNewFeatureInput = function() {
    $newFeature = $("<div></div>");
    $newFeature.prop("class", "panel panel-default col-md-12");
    $newFeature.html(
             + "<div class = \"row\">"
                + "<label name = \"features[" + featuresNb + "].priority\""
                + " class = \"col-md-4\">"
                    + "Priority :"
                + "</label>"
                + "<select name = \"features[" + featuresNb + "].priority\""
                + " class = \"col-md-2 form-control\">"
                + "</select>"
            + "</div>"
            + "<div class = \"row col-md-12\">"
                + "<label name = \"features[" + featuresNb + "].description\""
                + " class = \"col-md-4\">"
                + "Description :"
                + "</label>"
                + "<textarea name = \"features[" + featuresNb + "].description\""
                + " class = \"col-md-8 form-control\""
                + " placeholder = \"Description\"/>"
            + "</div>"
            + "<div class = \"row col-md-12\">"
                + "<label name = \"features[" + featuresNb + "].functionnalRequirements\""
                + " class = \"col-md-4\">"
                + "Functionnal requirements :"
                + "</label>"
                + "</div>"
                + "<div class = \"functionnalRequirements\""
                + " class = \"col-md-12\">"
            + "</div>"
    );
    var $select = $newFeature.find("select[name = \"features[" + featuresNb + "].priority\"]");
    var priorities = ["High", "Average", "Low"];
    $.each(priorities, function(index, elt) {
        var $option = $("<option></option>");
        $option.prop("value", elt)
        $option.html(elt);
        $select.append($option);
    });

    $fr = $newFeature.find(".functionnalRequirements");
    var moreFunctionnalRequirements = new MoreOfThisButton($fr, createNewFunctionnalRequirementInput);
    ++featuresNb;
    return $newFeature;
};

var moreFeatures = new MoreOfThisButton($("#features"), createNewFeatureInput);

