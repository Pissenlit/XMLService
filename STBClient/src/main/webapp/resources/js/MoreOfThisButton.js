/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var MoreOfThisButton = function($parent, createMoreFunction) {
    this.createMoreFunction = createMoreFunction;
    this.$parent = $parent;
    this.$button = this._createButton();   
    this.$parent.append(this.$button);
    this.$parent.append(createMoreFunction()); 
};

MoreOfThisButton.prototype.getButton = function() {
    return this.$button;
};

MoreOfThisButton.prototype._manageClick = function() {
    var $newElement = this.createMoreFunction();
    var $newEltContainerRow = $("<div></div>");
    $newEltContainerRow.prop("class", "row");
    var $newEltContainerCol = $("<div></div>");
    $newEltContainerCol.prop("class", "col-md-12");
    $newEltContainerCol.append($newElement);
    $newEltContainerRow.append($newEltContainerCol);
    this.$parent.append($newEltContainerRow);
    return false;
};

MoreOfThisButton.prototype._createButton = function() {
    var _this = this;
    var $buttonContainerRow = $("<div></div>");
    $buttonContainerRow.prop("class", "row");
    var $buttonContainerCol = $("<div></div>");
    $buttonContainerCol.prop("class", "col-md-12");
    var $button = $("<button></button>");
    $button.html("<i class = \"glyphicon glyphicon-plus\"> </i>")
    $button.prop("class", "btn btn-primary moreButton");
    $button.on("click", function() {
        return _this._manageClick();
    });
    $buttonContainerCol.append($button);
    $buttonContainerRow.append($buttonContainerCol);
    return $buttonContainerRow;
}
