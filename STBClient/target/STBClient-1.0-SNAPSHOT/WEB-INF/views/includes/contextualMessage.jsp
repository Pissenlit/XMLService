<%-- 
    Document   : contextualMessage
    Created on : May 6, 2016, 2:51:59 PM
    Author     : Axelle Boucher
--%>

<%@page import="stbcore.exchange.StbError"%>

<%
    StbError error = (StbError) request.getAttribute("error");
    if (error != null) {
        String errorMessage = error.getErrorMessage();    
        pageContext.setAttribute("errorMessage", errorMessage);
    }
%>

<!-- Display error message if necessary -->
<c:if test = "${errorMessage != null}">
    <div class = "alert alert-warning">
        <p>
            <strong><c:out value = "Error ! "/></strong>
            <c:out value = "${errorMessage}"/>
        </p>
    </div>       
</c:if>
        
<!-- Display success message if necessary -->
<c:if test = "${successMessage != null}">
    <div class = "alert alert-success">
        <p>
            <strong><c:out value = "Success ! "/></strong>
            <c:out value = "${successMessage}"/>
        </p>
    </div>       
</c:if>
        
        