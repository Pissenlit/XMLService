<%-- 
    Document   : header.jsp
    Created on : May 2, 2016, 2:26:52 PM
    Author     : Axelle Boucher
--%>

<header id = "header" 
     class = "navbar navbar-default col-md-12" 
     style="padding-top:7px">

    <div class = "row">
        <div id = "siteName" class = "col-md-2">
            <a href = "${pageContext.request.contextPath}/">
                <button class = "btn btn-default"> <i class = "glyphicon glyphicon-home"> Home</i> </button>
            </a>
        </div>

        <form action="${pageContext.request.contextPath}/search" 
              method = "POST"
              name = "searchForm"
              id = "searchBar"
              class = "col-md-6 form-inline">
            <div class= "form-group has-feedback">
                <input type = "text" 
                       name = "searchContent" 
                       placeholder = "Search"
                       class = "col-md-6 form-control"/>
                <i class = "glyphicon glyphicon-search form-control-feedback "></i>
            </div>
        </form>

        <div class = "col-md-2 col-md-offset-2 dropdown">
            <button class="btn btn-primary dropdown-toggle" 
                    type="button" 
                    data-toggle="dropdown">
                Menu
                <span class="caret"></span>
            </button>
            <ul class = "dropdown-menu">
                <li>
                    <div id = "viewAllSTBsMenuItem">
                        <a href = "${pageContext.request.contextPath}/viewSTB/all">View all STBs</a>
                    </div>
                </li>
                <li>
                    <div id = "insertSTBMenuItem">
                        <a href = "${pageContext.request.contextPath}/insertSTB">Insert STB</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</header>              
