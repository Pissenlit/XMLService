<%-- 
    Document   : viewSTB
    Created on : Apr 30, 2016, 8:16:28 PM
    Author     : Axelle Boucher
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="stbcore.model.Stb"%>

<%
    Stb stb = (Stb) request.getAttribute("stb");
    pageContext.setAttribute("stb", stb);
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@include file="includes/includes.jsp" %>
        <title>View STB</title>
    </head>
    <body>
        
        <div id = "page" class = "container">
            <%@include file="includes/header.jsp" %>
            
            <%@include file="includes/contextualMessage.jsp" %>
            
            <!-- Fields view --> 
            <div class = "col-md-6">
                <h1>Fields</h1>
                
                <!-- General fields -->
                <div class = "col-md-12">
                    <h3>General</h3>  
                </div>
                <div class = "row">
                    <div class= "col-md-4">Id :</div>
                    <div class= "col-md-6"><c:out value="${stb.getId()}"/></div>
                </div >
                <div class = "row">
                    <div class= "col-md-4">Title :</div>
                    <div class= "col-md-6"><c:out value="${stb.getTitle()}"/></div>
                </div >
                <div class = "row">
                    <div class= "col-md-4">Version :</div>
                    <div class= "col-md-6"><c:out value="${stb.getVersion()}"/></div>
                </div >
                <div class = "row">
                    <div class= "col-md-4">Date :</div>
                    <div class= "col-md-6"><c:out value="${stb.getDate()}"/></div>
                </div >
                <div class = "row">
                    <div class= "col-md-4">Description :</div>
                    <div class= "col-md-6"><c:out value = "${stb.getDescription()}"/></div>
                </div >
                <hr>
                
                <!-- Client related fields -->
                <div class = "col-md-12">
                    <h3>Client</h3>  
                </div>
                <div class="row">
                    <div class= "col-md-4">Entity :</div>
                    <div class= "col-md-6"><c:out value = "${stb.getClient().getEntity()}"/></div>
                </div>
                <div class="row">
                    <div class= "col-md-4">Zip code :</div>
                    <div class= "col-md-6"><c:out value = "${stb.getClient().getZipcode()}"/></div>
                </div>
                <div class="row">
                    <div class= "col-md-4">Contact first name :</div>
                    <div class= "col-md-6"><c:out value = "${stb.getClient().getContact().getFirstName()}"/></div>
                </div>
                <div class="row">
                    <div class= "col-md-4">Contact last name :</div>
                    <div class= "col-md-6"><c:out value = "${stb.getClient().getContact().getLastName()}"/></div>
                </div>
                <hr>
                
                <!-- Team related fields -->
                <div class = "col-md-12">
                    <h3>Team</h3>  
                </div>
                <c:forEach items = "${stb.getTeam().getMembers()}" var = "member">
                    <div><c:out value = "${member.getFirstName()} ${member.getLastName()}"/></div>
                </c:forEach>
                
                <!-- Features related fields -->
                <hr>
                <div class = "col-md-12">
                    <h3>Features</h3>  
                </div>
                <c:forEach items="${stb.getFeatures()}" var = "feature">
                    <div class = panel panel-default>
                        <div class="panel-heading">
                            <h4><c:out value = "${feature.getDescription()} (${feature.getPriority()})"/></h4>
                        </div>
                        <div class="panel-body">
                            <table class = "table table-striped">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Description</th>
                                        <th>Priority</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${feature.getFunctionnalRequirements()}" var = "fr">
                                    <tr>
                                        <td><c:out value = "${fr.getId()}"/></td>
                                        <td><c:out value = "${fr.getDescription()}"/></td>
                                        <td><c:out value = "${fr.getPriority()}"/></td>
                                    </tr>
                                    </c:forEach> 
                                </tbody>
                            </table>
                        </div>
                    </div>
                </c:forEach>
                <hr>
                 
            </div>
                
            <div class = "col-md-6">
                <h1>XML</h1>
                <div class = "well code code-xml">
                    <c:out value = "${stbXML}"/>
                </div>
            </div>
                
        </div>
    </body>
</html>
