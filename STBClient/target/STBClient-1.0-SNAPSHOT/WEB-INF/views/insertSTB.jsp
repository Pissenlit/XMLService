<%-- 
    Document   : insertSTB
    Created on : Apr 30, 2016, 6:52:09 PM
    Author     : Axelle Boucher
--%>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.lang.String"%>

<%
    String stbTemplate = (String) request.getAttribute("stbTemplate");
    pageContext.setAttribute("stbTemplate", stbTemplate);
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@include file="includes/includes.jsp" %>
        <title>Insert a STB</title>
    </head>
    <body>  
        <div id = "page" class = "container">
            <%@include file="includes/header.jsp" %>
            
            <%@include file="includes/contextualMessage.jsp" %>
            
            <div class = "row">
                <h1 class = "col-md-offset-4">Create a new STB</h1>
            </div>
            
            <div class = "row">
                
                <div id="insertSTBWithForm" class =  "col-md-6">
                    
                    <h2>Fill this form : </h2>          
                    
                    <sf:form method = "POST" action = "${pageContext.request.contextPath}/insertSTB/form" 
                                name = "STBInsertionForm" 
                                modelAttribute="stb" 
                                id ="STBInsertionForm"
                                class = "form-inline">
                        <div class = "row">
                            <sf:label path="title" 
                                          class = "col-md-4">
                                    Title :
                            </sf:label>                            
                            <sf:input path = "title" 
                                      class = "col-md-6 form-control"
                                      placeholder = "Title"/>
                        </div>
                        <div class = "row">
                            <sf:label path="version"
                                      class = "col-md-4">
                                Version : 
                            </sf:label>
                            <sf:input path = "version" 
                                      class = "col-md-8 form-control"
                                      placeholder = "xx.xx.{...}.xx"/>
                        </div>
                        <div class = "row">
                            <sf:label path="date" 
                                   class = "col-md-4">
                                Date : 
                            </sf:label>
                            <sf:input path = "date"
                                   class = "col-md-8 form-control"
                                   placeholder = "yyyy-MM-dd"/> 
                        </div>
                        <div class = "row">
                            <sf:label path="description"
                                      class = "col-md-4">
                                Description : 
                            </sf:label>
                            <sf:textarea path = "description" 
                                     class = "col-md-8 form-control"
                                     placeholder = "Description"/>
                        </div>
                        
                        <hr>
                        
                        <div class = "form-group row">
                            <sf:label path = "client"
                                      class = "col-md-12">
                                Client  
                            </sf:label>
                            <div class = "form-group row col-md-12">
                                <sf:label path = "client.zipcode"
                                          class = "col-md-4">
                                    Entity : 
                                </sf:label>
                                <sf:input path = "client.entity"
                                          class = "col-md-8 form-control"/>
                            </div>
                            <div class = "form-group row col-md-12">
                                <sf:label path = "client.zipcode"
                                          class = "col-md-4">
                                    Zip code : 
                                </sf:label>
                                <sf:input path = "client.zipcode"
                                          class = "col-md-8 form-control"/>
                            </div>
                            <div class = "form-group row col-md-12">
                                <sf:label path = "client.contact.firstName"
                                          class = "col-md-4">
                                    Contact first name : 
                                </sf:label>
                                <sf:input path = "client.contact.firstName"
                                          class = "col-md-8 form-control"/>
                            </div>
                            <div class = "form-group row col-md-12">
                                <sf:label path = "client.contact.lastName"
                                          class = "col-md-4">
                                    Contact last name : 
                                </sf:label>
                                <sf:input path = "client.contact.lastName"
                                          class = "col-md-8 form-control"/>
                            </div>
                        </div>
                            
                        <hr>
                        
                        <div class = "row">
                            <sf:label path = "team"
                                      class = "col-md-12">
                                Team
                            </sf:label>
                        </div>
                        <div class = "row" id = "team">
                            <!-- Javascriptly created -->                          
                        </div>
                        
                        <hr>
                        
                        <div class = "row">
                            <sf:label path = "features"
                                      class = "col-md-12">
                                Features
                            </sf:label>
                        </div>
                        <div class = "row" id = "features">
                            <!-- Javascriptly created -->                          
                        </div>
                        
                        <input type = "submit" 
                               class = "btn btn-primary col-md-2"
                               value = "Insert"/>
                        
                    </sf:form>
                </div>

                <div id = "insertSTBWithDirectInput"
                     class = "col-md-6">
                    <h2>Or use direct input : </hé>
                    <form method = "POST" 
                          action = "${pageContext.request.contextPath}/insertSTB/directInput" 
                            name = "STBInsertionDirectInput" 
                            id ="STBInsertionDirectInput"
                            class = "form">
                        
                        <input type = "submit"
                               class = "btn btn-primary col-md-offset-10 col-md-2"
                               value = "Insert"/>
                        
                        <textarea id = "stbInput"
                                  class = "form-control code well"
                                  name = "stbInput"
                                  rows = "50">
                            <c:out value = "${stbTemplate}"/>
                        </textarea>
                    </form>
                </div>
                
            </div>       

        </div>
    </body>
</html>

<script src="${pageContext.request.contextPath}/resources/js/insertSTB.js"></script>